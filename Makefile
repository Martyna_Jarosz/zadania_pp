CC=gcc
CFLAGS=-Wall
LIBS=-lm

mediana:
	$(CC) -c -o $@ mediana.c $(CFLAGS) $(LIBS)

odchylenie:
	$(CC) -c -o $@ odchylenie.c $(CFLAGS) $(LIBS)

srednia:
	$(CC) -c -o $@ srednia.c $(CFLAGS) $(LIBS)

8zad_1: 
	$(CC) -c -o $@ 8zad_1.c $(CFLAGS) $(LIBS)

final8zad_1: odchylenie.o srednia.o mediana.o 8zad_1.o
	$(CC) -o final8zad_1 8zad_1.o srednia.o odchylenie.o mediana.o $(CFLAGS) $(LIBS)

8zad_22: 
	$(CC) -c -o $@ 8zad_2.c $(CFLAGS) $(LIBS)

final8zad_2: odchylenie.o srednia.o mediana.o 8zad_2.o
	$(CC) -o final8zad_2 8zad_2.o srednia.o odchylenie.o mediana.o $(CFLAGS) $(LIBS)

all: clean final8zad_1 final8zad_2

run1: all
	./final8zad_1

run2: all
	./final8zad_2

clean:
	 del /f final8zad_1 final8zad_2 8zad_1.o 8zad_2.o srednia.o odchylenie.o mediana.o