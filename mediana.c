#include <stdio.h>
#include "funkcje.h"

void sortowanie(int *x, int n)   // n - ilosc liczb

{    int i=0, j=0, temp=0;          // temp - zmienna tymczasowa
    
    while(1)
    {
       j = 0;
       
       for(i=0 ; i<n-1 ; i++)
     {              
        if(x[i]>x[i+1])         // jesli liczba na 1 pozycji jest wieksza niz ta na 2
        {
          temp = x[i];
          x[i] = x[i+1];
          x[i+1] = temp;
          j = 1;
        }
        
     }     
        if(j==0)
        {
            break;
        }
    }
}

float mediana(float x[], int n)
{

    float mediana=0;       
    
    if(n%2 == 0)     
    {
        mediana = x[(n-1)/2] + x[n/2];              // liczenie mediany jesli ilosc liczb jest parzysta
        mediana = mediana/2;
    }
    else
    {
        mediana = x[(n+1)/2];                           // mediana jesli ilosc liczb jest nieparzysta 
    }

    return mediana;
}