#include <math.h>
#include "funkcje.h"

float odchylenie (float x[], float elem, float sred)
{
    int i=0;
    float odch = 0.0;

    for(i=0; i< elem; i++)
    {
        odch += pow((x[i] - sred), 2);
    }

    odch = sqrt(odch / elem);

    return odch;
}
